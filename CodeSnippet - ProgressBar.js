<!DOCTYPE html>


<style>
.progressBar {
    display: inline-block;
    width: 75%;
    border-radius: 15px;
    background-color: #305E64;
}
.progressBarContainer {
    width:100%;
    margin-bottom: 2vh;
}
.progressBarContainer:last-of-type {
    margin-bottom: 0vh;
}
.progressBarContainer > span {
    display: inline-block;
    width: 25%;
    font-size: 1.3em;
    font-weight: bold;
}
.progressBarInner {
    font-size: 1.2em;
    font-weight: bold;
    color: #161618;
    padding-left: 15px;  
    height: 100%;
    background-color: #4A9083;
    border-radius: 15px;
}
</style>

<div class="progressBarContainer">
    <span>C#</span><div class="progressBar"><div class="progressBarInner" style="width: 60%">60%</div></div>
</div>